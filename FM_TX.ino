// Small interface to the VMR6512 FM TX Module
// fabLAB Asturias 2015

// include the library code:
#include <LiquidCrystal.h>

// Defines
#define FREQ_MIN  880
#define FREQ_MAX 1080

#define LCD_RS  7
#define LCD_EN  6
#define LCD_D4  2
#define LCD_D5  3
#define LCD_D6  4
#define LCD_D7  5
#define LCD_COLS  16
#define LCD_ROWS  2

#define AIR_BTN  8
#define DIAL_IN  0
#define AIR_LED  9


// Variables
uint8_t on_air = 0;
uint16_t freq = 1000;
uint16_t new_freq;
uint16_t dial_value;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);



void on_air_pwr()
{
  // send full power command to FM chip
  Serial.write(0x02);
  Serial.write(0x00);
  Serial.write(0x73);
}

void off_air_pwr()
{
  // send stand-by power command to FM chip
  Serial.write(0x02);
  Serial.write(0x00);
  Serial.write(0x00);
}

void set_freq(int freq)
{
  // Set frequency of VMR6512 module
  // Freq = 10 * (D1*256 + D0)
  uint8_t d0, d1;
  
  d1 = (freq*10)/256;
  d0 = (freq*10)%256;
  
  // send command
  Serial.write(0x01);
  Serial.write(d1);
  Serial.write(d0);
  
  
}

void setup() {
  
  // init serial
  Serial.begin(19200);
  
  // FM module stand-by
  off_air_pwr();

  // pins
  pinMode(AIR_BTN, INPUT);
  digitalWrite(AIR_BTN, HIGH); // pullup
  pinMode(AIR_LED, OUTPUT);
  digitalWrite(AIR_LED, HIGH);

  // set up the LCD's number of columns and rows: 
  lcd.begin(LCD_COLS, LCD_ROWS);

  // Print initial message to the LCD.
  lcd.print("FM TX V0.1");
  lcd.setCursor(0,1);
  lcd.print("FabLAB Asturias");

  delay(3000);
  lcd.clear();

  lcd.print(freq/10.0,1);
  lcd.setCursor(6, 0);
  lcd.print("MHz");
  lcd.setCursor(0,1);
  lcd.print("Off Air");
  
}

void loop()
{
  if (!on_air)
  {
    dial_value = analogRead(DIAL_IN);
    new_freq = map(dial_value, 0, 1023, FREQ_MIN, FREQ_MAX);

    if (new_freq != freq)
    {
      freq=new_freq;

      lcd.setCursor(0, 0);
      lcd.print("     ");
      lcd.setCursor(0, 0);
      lcd.print(freq/10.0,1);
    }

    if (digitalRead(AIR_BTN) == 0)
    {
      delay(200);
      set_freq(freq);
      // on air
      digitalWrite(AIR_LED, 0);
      lcd.setCursor(0,1);
      lcd.print("ON Air ");
      // module full power
      on_air_pwr();
      on_air = 1;
    }
  }

  if (on_air)
  {
    if (digitalRead(AIR_BTN) == 0)
    {
      delay(200);
      lcd.setCursor(0,1);
      lcd.print("Off Air");
      digitalWrite(AIR_LED, 1);
      
      // module stand-by power
      off_air_pwr();
      on_air = 0;
    }
  }



}


