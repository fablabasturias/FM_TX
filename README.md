Simple interface to the VMR6512 FM TX Module with Arduino.
Uses an LCD Display, a potentiometer to set frequency, and a button to turn on RF power. A LED shows the status of RF Power.

